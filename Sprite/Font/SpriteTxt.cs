using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Sprite
{
	public class SpriteTxt : AbstractSprite
	{
		protected SpriteFont _font;
		protected string _txt;

		public string Txt { get { return _txt; } set { _txt = value; this.InitTaille(); } }

		public SpriteTxt(SpriteFont font, string txt)
		{
			_font = font;
			_txt = txt;

			this.Init();
		}

		protected override void Init()
		{
			base.Init();

			this.InitTaille();
		}

		public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			if (Show)
			{
				spriteBatch.DrawString(_font, _txt, Pos, _color, _rotation, _origin, Scale, _spriteEffect, 0);
			}
		}


		public void Draw(SpriteBatch spriteBatch, GameTime gameTime, string txt)
		{
			if (Show)
			{
				spriteBatch.DrawString(_font, txt, Pos, _color, _rotation, _origin, Scale, _spriteEffect, 0);
			}
		}


		/// <summary>
		/// (R�)initialise la taille du sprite
		/// </summary>
		protected void InitTaille()
		{
			Width = _font.MeasureString(_txt).X / Scale;
			Height = _font.MeasureString(_txt).Y / Scale;
			_origin = _font.MeasureString(_txt) / 2;
		}
	}
}