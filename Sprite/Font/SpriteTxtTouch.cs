using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Sprite
{
	public class SpriteTxtTouch : SpriteTxt
	{
		private Touch _touch;
		public Touch Touch { get { return _touch; } }

		public SpriteTxtTouch(SpriteFont font, string txt)
			: base(font, txt)
		{
		}

		protected override void Init()
		{
			base.Init();

			_touch = new Touch();
		}

		public override void Update(GameTime gameTime)
		{
			_touch.UpdateState(Box);

			base.Update(gameTime);
		}
	}
}