using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace Sprite
{
	public class Touch
	{
		public enum TouchState { Pressed, Up, Down, JustRealeased, Clicked };

		public TouchState _state;
		protected int _paddingBoxW;
		protected int _paddingBoxH;
		protected TouchLocation _lastTouch;
		protected bool _lastTouchBool;

		// R�cup�ration de l'�tat
		public bool isPressed { get { return _state == TouchState.Pressed; } }									// premier touch
		public bool isTouched { get { return _state == TouchState.Pressed || _state == TouchState.Down; } }	// touch
		public bool isJustRealeased { get { return _state == TouchState.JustRealeased; } }						// premier release si non cliqu�
		public bool isRealeased { get { return _state == TouchState.Up; } }									// released
		public bool isClicked { get { return _state == TouchState.Clicked; } }									// click�
		public bool isJustRealeasedOrClicked { get { return _state == TouchState.Clicked || _state == TouchState.Pressed; } }									// click�


		public Touch()
		{
			_paddingBoxW = 2;
			_paddingBoxH = 2;

			this.Init();
		}
		public Touch(int paddingW, int paddingH)
		{
			_paddingBoxW = paddingW;
			_paddingBoxH = paddingH;

			this.Init();
		}

		protected void Init()
		{
			_state = TouchState.Up;
			_lastTouchBool = false;
		}


		/// <summary>
		/// Update � appeler pour raffraichir le touch
		/// </summary>
		/// <param name="box">Rectangle de collision</param>
		public void UpdateState(Rectangle box)
		{
			TouchCollection touchCollection = TouchPanel.GetState();
			if(touchCollection.Count > 0)
			{
				TouchLocation touchLocation = touchCollection[0];
				_lastTouch = touchLocation;
				_lastTouchBool = true;
				Rectangle touchRect = new Rectangle((int)touchLocation.Position.X - _paddingBoxW, (int)touchLocation.Position.Y - _paddingBoxH, _paddingBoxW * 2 + 1, _paddingBoxH * 2 + 1);

				// si touch
				if ( (touchLocation.State == TouchLocationState.Pressed || touchLocation.State == TouchLocationState.Moved) && touchRect.Intersects(box))
				{
					// si premier touch
					if (_state == TouchState.Up || _state == TouchState.Clicked || _state == TouchState.JustRealeased)
						_state = TouchState.Pressed;
					else // sinon
						_state = TouchState.Down;
				}
				else
				{
					// si pr�cedemment touch�
					if (_state == TouchState.Down || _state == TouchState.Pressed)
					{
						// si relach� sur l'objet
						if (touchRect.Intersects(box))
							_state = TouchState.Clicked;
						else // si relach� en dehors de l'objet
							_state = TouchState.JustRealeased;
					}
					else
					{
						// on remet l'�tat initial
						_state = TouchState.Up;
					}
				}
			}
			else
			{
				if (_lastTouchBool)
				{
					if (new Rectangle((int)_lastTouch.Position.X - _paddingBoxW, (int)_lastTouch.Position.Y - _paddingBoxH, _paddingBoxW * 2 + 1, _paddingBoxH * 2 + 1).Intersects(box))
					{
						_state = TouchState.Clicked;
					}
					else
					{
						_state = TouchState.Up;
					}

					_lastTouchBool = false;
					_lastTouch = new TouchLocation();
				}
				else // on remet l'�tat initial
				{
					_state = TouchState.Up;
				}
			}
		}


		/// <summary>
		/// Permet de simuler un clic
		/// </summary>
		public void Click()
		{
			_state = TouchState.Clicked;
		}
	}
}
