using Microsoft.Xna.Framework.Graphics;

namespace Sprite
{
	public class Frame
	{
		protected Texture2D _texture;
		protected int _num;
		protected double _frameRate; // millisecondes

		public int Num { get { return _num; } }
		public double FrameRate { get { return _frameRate; } set { if (value > 0) _frameRate = value; } }

		public Frame(Texture2D texture, int num)
		{
			_texture = texture;
			_num = num;
			_frameRate = 400;
		}

		public Frame(Texture2D texture, int num, int frameRate)
		{
			_texture = texture;
			_num = num;
			_frameRate = frameRate;
		}
	}
}
