using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Linq;

namespace Sprite
{
	public class SpriteAnimated : AbstractSprite
	{
		protected Dictionary<string, FrameSet> _frameSets;

		protected FrameSet _currentFrameSet;
		protected double _lastFrameTime;

		public FrameSet CurrentFrameSet { get { return _currentFrameSet; } }

		public SpriteAnimated(Dictionary<string, FrameSet> frameSets)
		{
			_frameSets = frameSets;

			this.Init();
		}
		public SpriteAnimated(string key,FrameSet frameSet)
		{
			_frameSets = new Dictionary<string, FrameSet> { { key, frameSet } };

			this.Init();
        }
        public SpriteAnimated(FrameSet frameSet)
        {
            _frameSets = new Dictionary<string, FrameSet> { { "base", frameSet } };

            this.Init();
        }

        protected override void Init()
		{
			base.Init();

			if (_frameSets.Count > 0) _currentFrameSet = _frameSets.First().Value; // Frameset par d�faut

			this.InitFrameSet(true);
		}

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);

			
			if (_currentFrameSet.NbFrames > 1)
			{
				// si loop ou Once non termin�
				if (_currentFrameSet.Loop == LoopType.Loop || (_currentFrameSet.Loop == LoopType.Once && _currentFrameSet.CurrentFrame.Num < _currentFrameSet.NbFrames - 1))
				{
					// Maj temps frame
					_lastFrameTime += gameTime.ElapsedGameTime.TotalMilliseconds;

					// Si temps frame d�pass�, remise � z�ro et passage � la prochaine frame
					if (_lastFrameTime > _currentFrameSet.CurrentFrame.FrameRate)
					{
						_lastFrameTime = 0;
						_currentFrameSet.Next();
					}
				}
			}
		}

		public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			if (Show)
			{
				spriteBatch.Draw(_currentFrameSet.Texture, Pos, _currentFrameSet.DestinationFrame, new Color(_color.R, _color.G, _color.B, _alphaValue), _rotation, _origin, _scale, _spriteEffect, 0);
			}
		}



		/// <summary>
		/// Changement de Framset
		/// </summary>
		/// <param name="newFramset">FrameSet de destination</param>
		public virtual void ChangeFrameSet(FrameSet newFramset, bool reset = true)
		{
			_currentFrameSet = newFramset;
			this.InitFrameSet(reset);
        }

        /// <summary>
        /// Changement de Framset
        /// </summary>
        /// <param name="newFramset">Num�ro du FrameSet de destination</param>
        public virtual void ChangeFrameSet(string newKey, bool reset = true)
        {
            if (!_frameSets.ContainsKey(newKey)) throw new KeyNotFoundException();

            _currentFrameSet = _frameSets[newKey];
            this.InitFrameSet(reset);
        }

        /// <summary>
        /// Changement de Framset
        /// </summary>
        /// <param name="newFramset">Num�ro du FrameSet de destination</param>
        public virtual void ChangeFrameSet(int num, bool reset = true)
        {
            _currentFrameSet = _frameSets.ElementAtOrDefault(num).Value;
            this.InitFrameSet(reset);
        }

        // Prochain FrameSet
        public void NextFrameSet()
		{
            _frameSets.GetEnumerator().MoveNext();
            this.ChangeFrameSet(_frameSets.GetEnumerator().Current.Value);
		}

		/// <summary>
		/// Initialise les donn�es du FrameSet
		/// </summary>
		protected virtual void InitFrameSet(bool reset)
		{
			Width = _currentFrameSet.W;
			Height = _currentFrameSet.H;
			_origin = new Vector2(Width, Height) / 2;

			if (reset)
			{
				_lastFrameTime = 0;
				_currentFrameSet.Reset();
			}
		}
	}
}