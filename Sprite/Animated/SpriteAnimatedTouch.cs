using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Sprite
{
	public class SpriteAnimatedTouch : SpriteAnimated
	{
		private Touch _touch;
		public Touch Touch { get { return _touch; } }

		public SpriteAnimatedTouch(Dictionary<string, FrameSet> frameSets)
			: base(frameSets)
		{
        }
        public SpriteAnimatedTouch(string key, FrameSet frameSet)
            : base(new Dictionary<string, FrameSet> { { key, frameSet } })
        {
        }
        public SpriteAnimatedTouch(FrameSet frameSet)
            : base(new Dictionary<string, FrameSet> { { "base", frameSet } })
        {
        }

        protected override void Init()
		{
			base.Init();

			_touch = new Touch();
		}

		public override void Update(GameTime gameTime)
		{
			_touch.UpdateState(Box);

			base.Update(gameTime);
		}
	}
}