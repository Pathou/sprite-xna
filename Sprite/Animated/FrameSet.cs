using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sprite
{
	public enum LoopType { None, Once, Loop };

	public class FrameSet
	{
		protected List<Frame> _frames;
		protected int _tailleW;
		protected int _tailleH;
		protected Texture2D _texture;

		protected LoopType _loop;
		protected Frame _currentFrame;
		protected Rectangle _destinationFrame;

		internal Rectangle DestinationFrame { get { return _destinationFrame; } }
		public int NbFrames { get { return _frames.Count; } }
		public Frame CurrentFrame { get { return _currentFrame; } }
		public LoopType Loop { get { return _loop; } set { _loop = value; } }
		public Texture2D Texture { get { return _texture; } }
		public int W { get { return _tailleW; } }
		public int H { get { return _tailleH; } }
		public bool IsFinished { get { return (Loop == LoopType.Once && _currentFrame.Num == NbFrames - 1); } } // D�fini si l'animation est termin�e (utilisable en mode no loop uniquement)

		public FrameSet(Texture2D texture, Point tailleFrame, LoopType loop = LoopType.None)
		{
			_texture = texture;
			_tailleW = tailleFrame.X;
			_tailleH = tailleFrame.Y;
			_loop = loop;

			this.Init();
		}

		protected void Init()
		{
			_frames = new List<Frame>();
			int nbFrames = _texture.Width / _tailleW;

			if (nbFrames == 1) this.Loop = LoopType.None; // 1 Frame = Pas de loop

#if (DEBUG)
			if (nbFrames < 1) throw new Exception("Erreur initialisation frame");
#endif

			for (int i = 0; i < nbFrames; i++)
			{
				_frames.Add(new Frame(_texture, i));
			}

			this.ChangeFrame(_frames.FirstOrDefault());
		}



		/// <summary>
		/// Accesseur de frames
		/// </summary>
		/// <param name="numFrame">Num�ro de Frame � acc�der</param>
		/// <returns>Retourne frame � acc�der</returns>
		public Frame getFrame(int numFrame)
		{
			if (numFrame < NbFrames)
				return _frames.ElementAt(numFrame);
			else
				return _frames.FirstOrDefault();
		}

		/// <summary>
		/// Appel la prochaine Frame
		/// </summary>
		public void Next()
		{
			int nextFrame = _currentFrame.Num + 1;

			// si derni�re Frame
			if (nextFrame >= NbFrames)
			{
				this.ChangeFrame(_frames.FirstOrDefault());
			}
			else
			{
				this.ChangeFrame(_frames.ElementAtOrDefault(nextFrame)); 
			}
		}

		/// <summary>
		/// Reset le frameSet
		/// </summary>
		public void Reset()
		{
			this.ChangeFrame(_frames.FirstOrDefault());
		}

		/// <summary>
		/// Modifie la frame en cours
		/// </summary>
		/// <param name="newFrame">Nouvelle frame</param>
		public void ChangeFrame(Frame newFrame)
		{
			if (_currentFrame != newFrame)
			{
				_currentFrame = newFrame;
				this.SetDestinationFrame();
			}
		}
		/// <summary>
		/// Modifie la frame en cours
		/// </summary>
		/// <param name="newFrame">Num�ro de la nouvelle frame</param>
		public void ChangeFrame(int newFrame)
		{
			if (_currentFrame != _frames.ElementAtOrDefault(newFrame))
			{
				_currentFrame = _frames.ElementAtOrDefault(newFrame);
				this.SetDestinationFrame();
			}
		}

		/// <summary>
		/// Modifie le rectangle destination pour affichage
		/// </summary>
		protected void SetDestinationFrame()
		{
			_destinationFrame = new Rectangle(_tailleW * CurrentFrame.Num, 0, _tailleW, _tailleH);
		}

		/// <summary>
		/// Permet de modifier le framerate de toutes les frames
		/// </summary>
		/// <param name="framerate"></param>
		public void ChangeFramerates(double framerate)
		{
			foreach (Frame frame in _frames)
			{
				frame.FrameRate = framerate;
			}
		}
	}
}
