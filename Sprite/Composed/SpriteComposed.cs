using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Sprite
{
    public class SpriteComposed : AbstractSprite
    {
        public override Vector2 Pos
        {
            get { return _pos; }
            set
            {
                _pos = value;
                if(_sprites.Count > 0)
                {
                    foreach (var sprite in _sprites.Values)
                    {
                        //sprite.Pos = new Vector2(sprite.Pos.X + value.X, sprite.Pos.Y + value.Y);
                        sprite.Pos += value;
                    }
                }
            }
        }

        protected Dictionary<string, AbstractSprite> _sprites;
        public Dictionary<string, AbstractSprite> Sprites { get { return _sprites; } }

        public SpriteComposed(Dictionary<string, AbstractSprite> sprites)
		{
            _sprites = sprites;
		}

		protected override void Init()
		{
			base.Init();
		}

		public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{

            foreach (var sprite in _sprites.Values)
            {
                sprite.Draw(spriteBatch, gameTime);
            }

        }

        /// <summary>
        /// Ajouter un sprite
        /// </summary>
        /// <param name="key"></param>
        /// <param name="sprite"></param>
        public AbstractSprite addSprite(string key, AbstractSprite sprite)
        {
            _sprites.Add(key, sprite);
            return _sprites[key];
        }

        /// <summary>
        /// Ajouter un sprite
        /// </summary>
        /// <param name="key"></param>
        /// <param name="sprite"></param>
        public AbstractSprite getSprite(string key)
        {
            if (_sprites.ContainsKey(key)) return _sprites[key];
            else return null;
        }

    }
}