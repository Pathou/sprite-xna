using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Sprite
{
    public abstract class AbstractSprite
    {
		protected enum State { Birth, Adult, Dying, Dead }

        protected double _lifetime;
		protected double _maxLifetime;
		protected State _state;

        protected Vector2 _pos;
        protected Color	_color;
		protected int _alphaValue;
        protected float _rotation;
        protected Vector2 _origin;
        protected float _scale;
		protected Vector2 _direction;
		protected float _speed;
		protected SpriteEffects _spriteEffect;
        protected bool _show;
        protected float _width;
        protected float _height;


		// accesseurs
		public virtual Vector2 Pos { get { return _pos; } set { _pos = value; } }
		public Color Color { get { return _color; } set { _color = value; } }
		public float Rotation { get { return _rotation; } set { _rotation = value; } }
        public float Scale { get { return _scale; } set { if(value > 0) _scale = value; } }
        public Vector2 Direction { get { return _direction; } set { _direction = Vector2.Normalize(value); } }
        public float Speed { get { return _speed; } set { _speed = value; } }
        public bool Show { get { return _show; } set { _show = value; } }
		public double MaxLifetime { get { return _maxLifetime; } set { if (value >= 0) _maxLifetime = value; } }
		public bool isKillable { get { return _state == State.Dead; } } // Renvoi si le sprite est � d�truire ou non
		public bool isAdult { get { return _state == State.Adult; } }

        // Box de collision
        public float Width { get { return _width; } set { _width = value * _scale; } }
        public float Height { get { return _height; } set { _height = value * _scale; } }
        public virtual Rectangle Box { get {return new Rectangle( (int)(Pos.X - Width / 2), (int)(Pos.Y - Height / 2), (int)Width, (int)Height ); } }

		// D�fauts
		protected Vector2 _defaultPosition;
		protected Color _defaultColor;
		protected float _defaultRotation;
		protected Vector2 _defaultDirection;
		protected float _defaultScale;
		protected float _defaultSpeed;

        public AbstractSprite()
        {
            //this.Init();
        }

        protected virtual void Init()
        {
			_lifetime = 0;
			MaxLifetime = 0;
			_state = State.Birth;

            Pos = Vector2.Zero;
            Direction = Vector2.Zero;
            Speed = 0;
            _color = Color.White;
			_alphaValue = 255;
            _rotation = 0;
            Scale = 1;
			_spriteEffect = SpriteEffects.None;
            Show = true;

			_defaultPosition = Pos;
			_defaultColor = _color;
			_defaultRotation = _rotation;
			_defaultDirection = Direction;
			_defaultScale = Scale;
			_defaultSpeed = Speed;
        }

        public virtual void Update(GameTime gameTime)
        {
			// Maj du cycle de vie
			_lifetime += gameTime.ElapsedGameTime.TotalMilliseconds;
			if (MaxLifetime != 0)
			{
				if (_lifetime > MaxLifetime && _state != State.Dead) { this.Kill(); }
			}

			// si naissance
			if (_state == State.Birth)
			{
				this.Appear(gameTime);
			}
			// si touch�
			else if (_state == State.Dying)
			{
				this.Disappear(gameTime);
			}

			if (Direction != Vector2.Zero && Speed != 0) Pos += Direction * Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			// Abstract
        }


		/// <summary>
		/// Permet de faire apparaitre le sprite avec effets
		/// </summary>
		protected virtual void Appear(GameTime gameTime)
		{
			_state = State.Adult;
		}

		/// <summary>
		/// Permet de faire disparaitre le sprite avec effets
		/// </summary>
		protected virtual void Disappear(GameTime gameTime)
		{
			_state = State.Dead;
		}

		/// <summary>
		/// Tue le Sprite
		/// </summary>
		public void Kill()
		{
			_state = State.Dying;
		}

		/// <summary>
		/// D�truit le Sprite sans animation de transition
		/// </summary>
		public void Destroy()
		{
			_state = State.Dead;
		}

        /// <summary>
        /// Retourne si oui ou non le sprite est en collision avec un autre sprite
        /// </summary>
        public virtual bool IsCollision(AbstractSprite sprite)
        {
            return Box.Intersects(sprite.Box);
        }

        /// <summary>
        /// Retourne si oui ou non le sprite est en collision avec une liste de sprite
        /// </summary>
        public bool IsCollisions(List<AbstractSprite> sprites)
        {
            bool collide = false;
            foreach (AbstractSprite sprite in sprites)
            {
                if (Box.Intersects(sprite.Box)) collide = true;
				break;
            }
            return collide;
        }
    }
}
