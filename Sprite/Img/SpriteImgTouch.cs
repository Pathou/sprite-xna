using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Sprite
{
	public class SpriteImgTouch : SpriteImg
	{
		private Touch _touch;
		public Touch Touch { get { return _touch; } }

		public SpriteImgTouch(Texture2D texture)
			: base(texture)
		{
		}

		protected override void Init()
		{
			base.Init();

			_touch = new Touch();
		}

		public override void Update(GameTime gameTime)
		{
			_touch.UpdateState(Box);

			base.Update(gameTime);
		}
	}
}
