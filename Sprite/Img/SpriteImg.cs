using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Sprite
{
	public class SpriteImg : AbstractSprite
	{
		protected Texture2D _texture;

		public SpriteImg(Texture2D texture)
		{
			_texture = texture;

			this.Init();
		}

		protected override void Init()
		{
			base.Init();

			Width = _texture.Width;
			Height = _texture.Height;
			_origin = new Vector2(Width, Height) / 2;
		}

		public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			if (Show)
			{
				spriteBatch.Draw(_texture, Pos, null, new Color(_color.R, _color.G, _color.B, _alphaValue), _rotation, _origin, _scale, _spriteEffect, 0);
			}
		}
	}
}